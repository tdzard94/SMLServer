var User = require('./models/tbUser');
var mongoose = require('mongoose');
var dbConfig = require('./config/database');
var CLIENT = require('./models/client');

mongoose.connect(dbConfig.url,(err)=>{
    if(err) console.log(err);
    console.log('Connected');
});

var _user = new User({
    username: 'tdzard94',
    password: new User().generateHash('Nayzard94!'),
    isAdmin: true,
    enable: true,
    apiKey: new User().generateapiKey()
});
_user.save((err,u)=>{
    if(err) return console.log(err);
    return console.log('done');
});
