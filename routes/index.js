var express = require('express');
var router = express.Router();
var passport = require('../config/passport');

router.use(passport.initialize());
router.use(passport.session());
/* GET home page. */
require('./login')(router,passport);
require('./admin/index')(router);
require('./api/index')(router);
module.exports = router;
