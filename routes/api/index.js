module.exports = (router)=>{
    require('./v1.1/client')(router);
    require('./v1.1/proxy')(router);
    require('./v1.1/devices')(router);
    require('./v1.1/scripts')(router);
}