var conf = require('../../config/conf');
var base64url = require('base64url');
var Users = require('../../models/tbUser');

module.exports = {
    authorization: async (req,res,next)=>{
        try{
            var user = await Users.findOne({apiKey: req.headers.apikey}).exec();
            if(user)
            {
                req.uid = user._id;
                next();
            }
               
            else
                res.json({code:405, message:'Access denied, contact to admin' });
        }catch(e)
        { 
            res.json({code:405, message:'Access denied, contact to admin' });
        }
         
    },
    direct: async (req,res,next)=>{
        try{
            var user = await Users.findOne({apiKey: req.query.apikey}).exec();
            if(user)
            {
                req.uid = user._id;
                next();
            }
               
            else
                res.json({code:405, message:'Access denied, contact to admin' });
        }catch(e)
        { 
            res.json({code:405, message:'Access denied, contact to admin' });
        }
    }
}