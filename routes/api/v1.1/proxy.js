var auth = require('../auth');
var CLIENT = require('../../../models/client');
var PROFILE = require('../../../models/profile');
var SSH = require('../../../models/ssh');
var mongodb = require('mongodb');
var intIp = require('ip-to-int');

var GROUP = require('../../../models/group');
module.exports = (router)=>{
    router.get('/v1.1/proxy/get',auth.authorization,async (req,res,next)=>{
        var cl = await CLIENT.findById(req.query.cid).select('group').exec();
        
        var proxy = await GROUP.findOne({userId: req.uid, name: cl.group}).select('proxy').exec();
        
        var ssh = await SSH.aggregate([
            { $match: {'_id': mongodb.ObjectId(proxy.proxy)}},
            {$unwind: '$sshs'},
            {$sample:{size: 1} }
        ]);
        return res.json({success:true, ssh:{ip: intIp(ssh[0].sshs.ip).toIP(),username: ssh[0].sshs.username,password: ssh[0].sshs.pass}})
    });
}