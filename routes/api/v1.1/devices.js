var auth = require('../auth');
var DV = require('../../../models/deviceinfo');
var CARRIER = require('../../../models/carrier');
module.exports = (router)=>{
    router.get('/v1.1/devices/get',auth.authorization,async (req,res,next)=>{
        var dv = await DV.aggregate([
            {$match: {}},
            {$sample: {size: 1}}
        ]);

        return res.json({success:true, device: dv[0]});
    });

    router.get('/v1.1/carrier/get',auth.authorization,async (req,res,next)=>{
        var dv = await CARRIER.aggregate([
            {$match: {ISO: req.query.code}},
            {$sample: {size: 1}}
        ]);

        return res.json({success:true, carrier: dv[0]});
    });
}