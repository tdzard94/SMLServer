var CLIENT = require('../../../models/client');
var PROFILLE = require('../../../models/profile');
var UA = require('../../../models/useragent');
var SSH = require('../../../models/ssh');
var RANGEIP = require('../../../models/rangeip');
var auth = require('../auth');
var intIp = require('ip-to-int');
var mongodb = require('mongodb');
var SETTINGS = require('../../../models/settings');
var GROUP = require('../../../models/group');

module.exports = (router)=>{
    router.post('/v1.1/client/register',auth.authorization,async (req,res,next)=>{
        var cl = await CLIENT.findOne({name: req.body.name}).exec();
        if(cl)
            return res.json({success:false, errMsg: "Name is exist"});
        
        cl = new CLIENT({
            userId: req.uid,
            name: req.body.name,
            IP: req.body.ip,
            group: "null",
            dateCreate: Date.now()
        });

        await cl.save();
        return res.json({success:true,cid: cl._id});
    });
    router.post('/v1.1/client/authenciation',auth.authorization,async (req,res,next)=>{
        var cl = await CLIENT.findOne({name: req.body.name}).exec();
        if(cl)
            return res.json({success:true,cid: cl._id});
        return res.json({success:false});
    });

    router.get('/v1.1/client/getsetting',auth.authorization,async (req,res,next)=>{
        var sets = await SETTINGS.findOne({userId: req.uid}).exec();

        if(!sets)
            return res.json({success:false});
        
        return res.json({success:true, data: sets});
    });

    router.get('/v1.1/client/getgid',auth.authorization,async (req,res,next)=>{
        var cl = await CLIENT.findById(req.query.cid).exec();

        if(!cl)
            return res.json({success:false});
        
        var gr = await GROUP.findOne({userId: req.uid, name: cl.group}).exec();
        if(!gr)
            return res.json({success:false});

        return res.json({success:true, data: gr._id});
    });

    router.get('/v1.1/client/getaction',auth.authorization,async (req,res,next)=>{
        var cl = await CLIENT.findById(req.query.cid).exec();

        if(!cl)
            return res.json({success:false});
        
            console.log(mongodb.ObjectId(cl._id).toString());
        var gr = await GROUP.findOne({userId: req.uid, name: cl.group}).exec();
        if(!gr)
            return res.json({success:false});

        return res.json({success:true, data: gr.action});
    });

    router.get('/v1.1/direct',auth.direct,async (req,res,next)=>{
        var cl = await CLIENT.findById(req.query.cid).exec();
        if(!cl)
            return res.json({success:false, errMsg: 'THIS CLIENT HAS NOT ACTIVE YET!'});

        var gr = await GROUP.findOne({userId: req.uid, name: cl.group}).exec();

        if(!gr)
            return res.json({success:false, errMsg: 'THIS CLIENT HAS NO GROUP'});
        
        var pr = await PROFILLE.findById(gr.profile).exec();
        if(!pr)
            return res.json({success:false, errMsg: 'GET PROFILE ERROR!'});
        return res.redirect(pr.url);
    });
    
}