var auth = require('../auth');
var SCRIPT = require('../../../models/script');
var mongodb = require('mongodb');

module.exports = (router)=>{
    router.get('/v1.1/script/get',auth.authorization,async (req,res,next)=>{
        var scr = await SCRIPT.aggregate([
            {$match: {userId: req.uid.toString(),package: req.query.package}},
            {$sample: {size:1}}
        ]).exec();

        if(scr.length > 0)
        {
            return res.json({success:true, script: scr[0].content});
        }

        scr = await SCRIPT.aggregate([
            {$match: {userId: req.uid.toString(), isDefalut: true}},
            {$sample: {size:1}}
        ]).exec();

        if(scr.length > 0)
        {
            return res.json({success:true, script: scr[0].content});
        }

        return res.json({success:true, script: ['randomclick: 100']});

    });
}