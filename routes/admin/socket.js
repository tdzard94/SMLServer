var conf = require('../../config/conf');
var base64url = require('base64url');

module.exports = async (io)=>{ 
    io.on('connection',(client)=>{
        console.log('new connection: '+client.id);
        client.on('register',(data)=>{
            console.log(data);
            switch(data.platform)
            {
                case "web_client":
                    clientPC.forEach(x=>{
                        io.emit('pc_online',{id: x.clientID});
                    });
                    break;
                case "pc_client":
                    clientPC.push({
                        socketid: client.id,
                        clientID: data.id
                    });
                    //client.emit('pc_online',{id: data.id});
                    io.emit('pc_online',{id: data.id});
                    break;
            }
        });
        
        client.on('disconnect',()=>{
            
            var index = clientPC.findIndex(x=>x.socketid == client.id);
            if(index > -1)
            {
                io.emit('pc_offline',{id: clientPC[index].clientID});
                
                clientPC.splice(index,1);
            }
                
        });

        client.on('control_action',(data)=>{
            var index = clientPC.findIndex(x=>x.clientID == data.id);
            console.log(index);
            if(index != -1)
                client.to(clientPC[index].socketid).emit('control_action',data.action);

        });

        client.on('send_action',(data)=>{
            console.log(data);  
            io.emit('client_action',data.action+"|"+data.id);
        });

        client.on('update_log',(data)=>{
            io.emit('log_news',data);
        });

    });

}

var clientPC = [{
    socketid: String,
    clientID:String
}];