var auth = require('../../config/auth');
var PROFILE = require('../../models/profile');

module.exports = (router)=>{
    router.get('/profile',auth.isAdmin,(req,res)=>{
        return res.render('admin/profile',{
            title: 'Zlick - Profile',
            active: 'profile'
        });
    });

    router.get('/profile/getdata',auth.isLogout,async (req,res,next)=>{
        var pr = await PROFILE.find({userId: req.user.id}).select(['name','url','network']).exec();
        return res.json({success:true, data: pr});
    });
    router.get('/profile/get/:id',auth.isLogout,async (req,res,next)=>{
        var pr = await PROFILE.findById(req.params.id).exec();
        if(!pr)
            return res.json({success:false});

        return res.json({success: true,data: pr});
    });

    router.post('/profile/add',auth.isLogout,async (req,res,next)=>{
        var pr = await PROFILE.findOne({userId: req.user.id,name: req.body.name}).exec();

        if(pr)
            return res.json({success:false,errMsg: "Name is not availble"});

        pr = new PROFILE({
            userId: req.user.id,
            name: req.body.name,
            url: req.body.url,
            network: req.body.network,
            focus: req.body.forcus,
        });
        await pr.save();
        return res.json({success:true, data: pr});
    });

    router.post('/profile/edit',auth.isLogout,async (req,res,next)=>{
        var pr = await PROFILE.findById(req.body.id).exec();
        if(!pr) return res.json({success:false});

        pr.url = req.body.url;
        pr.sshs = req.body.sshs;
        pr.rangeip = req.body.rangeip;
        pr.platform = req.body.platform;
        await pr.save();
        return res.json({success:true});
    });

    router.post('/profile/delete',auth.isLogout,async (req,res,next)=>{
        var pr = await PROFILE.findByIdAndRemove(req.body.id).exec();
        if(!pr)
            return res.json({success:false});
        return res.json({success:true});
    });

    router.post('/profile/reset',auth.isLogout,async (req,res,next)=>{
        var pr = await PROFILE.findById(req.body.id).exec();
        if(!pr) return res.json({success:false});

        pr.count = 0;
        await pr.save();
        return res.json({success:true});
    });
}