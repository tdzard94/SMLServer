var auth = require('../../config/auth');
var mongodb = require('mongodb');
var GROUP = require('../../models/group');
var CLIENT = require('../../models/client');
var PROFILE = require('../../models/profile');
var SSH = require('../../models/ssh');

module.exports = (router)=>{
    router.get('/dashboard',auth.isAdmin,(req,res)=>{
        return res.render('admin/dashboard',{
            title: 'HLServer - DASHBOARD',
            active: 'dashboard'
        });
    });

    router.get('/dashboard/getdata',auth.isLogout, async (req,res,next)=>{
        var gr = await GROUP.find().exec();
        if(!gr)
            return res.json({success:false});
        return res.json({success:true,data: gr});
    });

    router.get('/dashboard/info',auth.isLogout,async (req,res,next)=>{
        var data = await Promise.all([PROFILE.find().select('name'),SSH.find().select('name'),CLIENT.find().select(['name','group'])]);
        return res.json({success:true,data: data});
    });

    router.get('/dashboard/get/:id',auth.isLogout,async (req,res,next)=>{
        var gr = await GROUP.findById(req.params.id).exec();
        if(!gr)
            return res.json({success:false});
        return res.json({success:true,data: gr});
    });

    router.post('/dashboard/add',auth.isLogout,async (req,res,next)=>{
        var gr = await GROUP.findOne({userId: req.user.id, name: req.body.name}).exec();
        if(gr)
            return res.json({success:false, errMsg: "Name is not availble"});

        gr = new GROUP({
            userId: req.user.id,
            name: req.body.name,
            profile: req.body.profile,
            proxy: req.body.proxy,
            useSocks: req.body.usesocks,
            status: "Created!",
            action: 'stop_lead'
        });

        await req.body.clients.forEach(e => {
            CLIENT.findByIdAndUpdate(e,{group: req.body.name}).exec();
        });

        await gr.save();
        return res.json({success:true, data:gr});
    });

    router.post('/dashboard/edit',auth.isLogout,async (req,res,next)=>{
        var gr = await GROUP.findById(req.body.id).exec();
        if(!gr)
            return res.json({success:false});

        var listClients = await CLIENT.find({userId: req.user.id, group: gr.name});
        await listClients.forEach(async x=>{
            x.group="null";
            await x.save();
        });

        
        gr.profile = req.body.profile;
        gr.proxy = req.body.proxy;
        await gr.save();
        await req.body.clients.forEach(async x=>{
            await CLIENT.findByIdAndUpdate(x,{group: gr.name}).exec();
        });
        return res.json({success:true});
    });

    router.post('/dashboard/delete',auth.isLogout,async (req,res,next)=>{
        var gr = await GROUP.findByIdAndRemove(req.body.id).exec();

        if(!gr)
            return res.json({success:false});

        var groupCl  = await CLIENT.find({userId: req.user.id, group: gr.name});
        await groupCl.forEach(x=>{
            x.group="null";
            x.save();
        });
        return res.json({success:true});
    });

    router.post('/dashboard/updateaction',auth.isLogout, async (req,res,next)=>{
        var gr = await GROUP.findByIdAndUpdate(req.body.id,{
            action: req.body.action
        }).exec();

        console.log(gr);
        return res.json({success:true});
    });

}