var auth = require('../../config/auth');
var SCR = require('../../models/script');

module.exports = (router)=>{
    router.get('/scripts',auth.isAdmin,async (req,res,next)=>{
        return res.render('admin/script',{
            title: 'ZSERER - SCRIPTS',
            active: 'script'
        });
    });

    router.get('/scripts/getlist',auth.isLogout,async (req,res,next)=>{
        var data = await SCR.find().select('name').exec();
        return res.json({success:true,data:data});
    });

    router.get('/scripts/get/:id',auth.isLogout,async (req,res,next)=>{
        var sc = await SCR.findById(req.params.id).exec();
        if(!sc)
            return res.json({success:false});

        return res.json({success:true, data:sc});
    });
    
    router.post('/scripts/add',auth.isLogout,async (req,res,next)=>{
        var sc = await SCR.findOne({userId: req.user.id, name: req.body.name}).exec();

        if(sc)
            return res.json({success:false});
        
        var scr = new SCR({
            userId: req.user.id,
            name: req.body.name,
            package: req.body.package,
            isDefault: req.body.df,
            content: req.body.content.split('\n')
        });

        await scr.save();
        return res.json({success:true, data: {name: scr.name,_id: scr._id}});
    });

    router.post('/scripts/edit',auth.isLogout,async (req,res,next)=>{
        var sc = await SCR.findById(req.body.id).exec();
        if(!sc) return res.json({success:false});

        sc.package = req.body.package;
        sc.isDefault = req.body.isDefault;
        sc.content = req.body.content.split('\n');
        await sc.save();
        return res.json({success:true});
    });

    router.post('/scripts/delete',auth.isLogout,async (req,res,next)=>{
        var sc = await SCR.findByIdAndRemove(req.body.id).exec();
        if(!sc)
            return res.json({success:false});
        return res.json({success:true});
    });
}