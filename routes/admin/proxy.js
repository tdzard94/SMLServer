var auth = require('../../config/auth');
var SSH = require('../../models/ssh');
var RANGE = require('../../models/rangeip');
var intIp = require('ip-to-int');

module.exports = (router)=>{
    router.get('/proxy/ssh',auth.isAdmin, async (req,res,next)=>{
        return res.render('admin/proxy',{
            title: 'Proxy - ssh',
            active: 'proxyssh',
            type: "ssh"
        });
    });

    router.get('/proxy/rangeip',auth.isAdmin, async (req,res,next)=>{
        return res.render('admin/proxy',{
            title: 'Proxy - Range IP',
            active: 'proxyrangeip',
            type: "rangeip"
        });
    });

    router.get('/proxy/get/:type',auth.isLogout,async (req,res,next)=>{
        switch(req.params.type)
        {
            case "ssh":
                var sshs = await SSH.find({userId: req.user.id}).select(['name','total']).exec();
                return res.json({success:true, data: sshs});
                break;
            case "rangeip":
                var sshs = await RANGE.find({userId: req.user.id}).select(['name','total']).exec();
                return res.json({success:true, data: sshs});
                break;
        }
    });

    router.get('/proxy/getlist',auth.isLogout,async (req,res,next)=>{
        var data = await Promise.all([SSH.find({userId: req.user.id}).select('name').exec(), RANGE.find({userId: req.user.id}).select('name').exec()]);
        return res.json({success:true,data:data});
    });

    router.get('/proxy/ssh/get/:id',auth.isLogout,async (req,res,next)=>{
        var ssh = await SSH.findById(req.params.id).exec();
        if(!ssh)
            return res.json({success:false});
        return res.json({success:true,data:ssh});
    });
    router.get('/proxy/rangeip/get/:id',auth.isLogout,async (req,res,next)=>{
        var ssh = await RANGE.findById(req.params.id).exec();
        if(!ssh)
            return res.json({success:false});
        return res.json({success:true,data:ssh});
    });
    router.post('/proxy/add',auth.isLogout,async (req,res,next)=>{
        switch(req.body.type)
        {
            case "ssh":
                var ssh = await SSH.findOne({name: req.body.name}).exec();
                if(ssh)
                    return res.json({success:true, isDup: true});

                var sshs = [];
                req.body.content.split('\n').forEach(e => {
                    try{
                        var data = e.split('|');
                        sshs.push({ip: intIp(data[0]).toInt(),username: data[1], pass: data[2]});      
                    }catch(e)
                    {

                    }
                });
                ssh = new SSH({
                    userId: req.user.id,
                    name: req.body.name,
                    total: sshs.length,
                    sshs: sshs
                });

                await ssh.save();
                return res.json({success:true, data: {_id: ssh._id,name: ssh.name, count: ssh.total}});
                break;
            case "rangeip":
                var range = await RANGE.findOne({name: req.body.name}).exec();
                if(range)
                    return res.json({success:true, isDup: true});
                var ranges = [];

                req.body.content.split('\n').forEach(e => {
                    try{
                        var data = e.split('|');
                        ranges.push({start: intIp(data[0]).toInt(),end: intIp(data[1]).toInt()}); 
                    }catch(e){}
                                     
                });
                range = new RANGE({
                    userId: req.user.id,
                    name: req.body.name,
                    total: ranges.length,
                    ranges: ranges
                });
                await range.save();
                return res.json({success:true, data: {_id: range._id,name: range.name, count: range.total}});
                break;
            default:
                return res.json({success:false, errMsg: "Cac du ma nha may"});
        }
    });

    router.post('/proxy/edit',auth.isLogout,async (req,res,next)=>{
        switch(req.body.type)
        {
            case "ssh":
                var ssh = await SSH.findById(req.body.id).exec();
                if(!ssh)
                    return res.json({success:false});

                var sshs = [];
                req.body.content.split('\n').forEach(e => {
                    try{
                        var data = e.split('|');
                        sshs.push({ip: intIp(data[0]).toInt(),username: data[1], pass: data[2]});      
                    }catch(e){}
                });
                ssh.sshs = sshs;
                ssh.total = sshs.length;
                await ssh.save();
                return res.json({success:true, count: sshs.length});
                break;
            case "ranngeip":
                var range = await RANGE.findById(req.body.id).exec();
                if(!range)
                    return res.json({success:false});
                
                var ranges = [];
                req.body.content.split('\n').forEach(e => {
                    try{
                        var data = e.split('|');
                        ranges.push({ip: intIp(data[0]).toInt(),useraname: data[1], pass: data[2]}); 
                    }catch(e){}
                                     
                });
                range.ranges = ranges;
                range.total = ranges.length;
                await range.save();
                return res.json({success:true,count: ranges.length});
                break;
        }
    });

    router.post('/proxy/delete',auth.isLogout,async (req,res,next)=>{
        switch(req.body.type)
        {
            case "ssh":
                var ssh = await SSH.findByIdAndRemove(req.body.id).exec();
                if(ssh)
                    return res.json({success:true, data: ssh._id});
                else return res.json({success:false});
                break;
            case "rangeip":
                var ssh = await RANGE.findByIdAndRemove(req.body.id).exec();
                if(ssh)
                    return res.json({success:true, data: ssh._id});
                else return res.json({success:false});
            break;
                break;
            default:
                return res.json({success:false, errMsg: "Cac du ma nha may"});
        }
    });
}