var auth = require('../../config/auth');
var SETTINGS = require('../../models/settings');


module.exports = (router)=>{
    router.get('/settings',auth.isAdmin, async (req,res,next)=>{
        return res.render('admin/settings',{
            title: 'Settings',
            active: 'settings',
        });
    });

    router.get('/settings/get',auth.isLogout,async (req,res,next)=>{
        var set = await SETTINGS.findOne({userId: req.user.id}).exec();
        if(!set)
            return res.json({success: true, data: null});
        return res.json({success: true, data: set});
    });

    router.post('/settings/save',auth.isLogout,async (req,res,next)=>{
        var set = await SETTINGS.findOne({userId: req.user.id}).exec();
        if(!set)
        {
            set = new SETTINGS({
                userId: req.user.id,
                TIME_LOADLINK: 0,
                TIME_DOWNAPP: 0,
                TEXT_CLICK: '',
                TEXT_ERROR: '',
                DEFAULT_PACKAGE: '',
                WIPE_PACKAGE: ''
            });
            await set.save();
        }

        set.TIME_LOADLINK = req.body.timeLoad;
        set.TIME_DOWNAPP = req.body.timeDown;
        set.TEXT_CLICK = req.body.click;
        set.TEXT_ERROR = req.body.error;
        set.DEFAULT_PACKAGE = req.body.dfpkg;
        set.WIPE_PACKAGE = req.body.wipepkg;
        await set.save();
        return res.json({success:true});
    });
}