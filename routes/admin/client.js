var auth = require('../../config/auth');
var CLIENT = require('../../models/client');

module.exports = (router)=>{
    router.get('/client',auth.isAdmin, async (req,res,next)=>{
        return res.render('admin/client',{
            title: 'ZCLICK - CLIENT MANAGER',
            active: 'client'
        });
    });

    router.get('/client/getdata',auth.isLogout,async (req,res,next)=>{
        var cl = await CLIENT.find({userId: req.user.id}).exec();
        return res.json({success: true,data:cl});
    });

    router.get('/client/get/:id',auth.isLogout, async (req,res,next)=>{
        var cl = await CLIENT.findById(req.params.id).exec();
        if(!cl)
            return res.json({success:false});
        
        return res.json({success:true,data: cl});
        
    });

    router.post('/client/add',auth.isLogout,async (req,res,next)=>{

    });

    router.post('/client/edit',auth.isLogout,async (req,res,next)=>{
        var cl = await CLIENT.findById(req.body.id).exec();
        if(!cl)
            return res.json({success:false});

        cl.profile = req.body.profile;
        cl.thread = req.body.thread;
        cl.port = req.body.port;
        await cl.save();
        return res.json({success:true});
    });

    router.post('/client/delete',auth.isLogout,async (req,res,next)=>{
        var cl = await CLIENT.findByIdAndRemove(req.body.id).exec();
        if(cl)
            return res.json({success:true});
        else
            return res.json({success:false});
    });

    router.post('/client/edit/profile',auth.isLogout,async (req,res,next)=>{
        
    });

}