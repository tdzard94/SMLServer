var auth = require('../../config/auth');
var UA = require('../../models/useragent');
module.exports = (router)=>{
    router.get('/useragent/android',auth.isAdmin,async (req,res,next)=>{
        return res.render('admin/useragent',{
            title: 'USERAGENT - Android',
            active: 'useragentandroid',
            type: "android"
        });
    });

    router.get('/useragent/ios',auth.isAdmin,async (req,res,next)=>{
        return res.render('admin/useragent',{
            title: 'USERAGENT - IOS',
            active: 'useragentios',
            type: "ios"
        });
    });

    router.get('/useragent/pc',auth.isAdmin,async (req,res,next)=>{
        return res.render('admin/useragent',{
            title: 'USERAGENT - PC',
            active: 'useragentpc',
            type: "pc"
        });
    });

    router.post('/useragent/add',auth.isLogout,async (req,res,next)=>{
        var ua = await UA.findOne({type: req.body.type}).exec();
        if(!ua)
        {
            ua = new UA({
                userId: req.user.id,
                type: req.body.type,
                useragents: []
            });
            await ua.save();
        }

        ua.useragents = req.body.content.split('\n');
        await ua.save();
        return res.json({success:true});
    });

    router.get('/useragent/get/:type',auth.isLogout,async (req,res,next)=>{
        var ua = await UA.findOne({type: req.params.type}).exec();
        if(!ua)
            return res.json({success:false});
        
        return res.json({success:true,data: ua.useragents});
    });
}