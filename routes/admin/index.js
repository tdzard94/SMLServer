module.exports = (router)=>{
    require('./dashboard')(router);
    require('./profile')(router);
    require('./client')(router);
    require('./proxy')(router);
    require('./settings')(router);
    require('./scripts')(router);
    require('./apikey')(router);
}