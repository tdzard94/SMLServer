var auth = require('../../config/auth');
var tbUser = require('../../models/tbUser');

module.exports = (router)=>{
    router.get('/api-key',auth.isAdmin, (req,res,next)=>{
        return res.render('admin/apikey',{
            title: 'API KEY - HLServer',
            active: 'apikey'
        });
    });

    router.get('/api-key/getkey',auth.isLogout,(req,res,next)=>{
        res.json({key: req.user.apiKey});
    });

    router.get('/api-key/genkey',auth.isLogout,(req,res,next)=>{
        tbUser.findById(req.user.id,(err,user)=>{
            user.apiKey = user.generateapiKey();
            user.save((err)=>{
                res.json({key: req.user.apiKey});
            });
        });
    });
}
