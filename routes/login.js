var auth = require('../config/auth');

module.exports = (router,passport)=>{
    router.get('/',(req,res)=>{
        if(req.isAuthenticated())
        {
            return res.redirect('/dashboard');
        }
        else
            return res.redirect('/login');
    });
    
    router.get('/login',async (req,res,next)=>{
        if(req.isAuthenticated())
        {
            return res.redirect('/dashboard');
        }
        else
        {
            return res.render('login',{
                title: 'HLServer-Login'
            });
        }
    });

    router.route('/login')
        .get(function (req, res) {
            if(req.isAuthenticated())
            {
                return res.redirect('/dashboard');
            }
            else
            {
                return res.render('login',{
                    title: 'HLServer-Login'
                });
            }
        })
        .post(function(req, res, next) {
            passport.authenticate('user-login', function(err, user, info) {
            if (err) {
                return next(err); // will generate a 500 error
            }
            if (!user) {
                return res.status(409).render('login', {mess: info.errMsg,username: info.username,password: info.password});
            }
            req.login(user, function(err){
                if(err){
                console.error(err);
                return next(err);
                }
                return res.redirect('/dashboard');
            });
        })(req, res, next);
        
    });

    router.get('/logout',(req,res,next)=>{
        req.logout();
        res.redirect('/');
    });

}