var mongoose = require('mongoose');

var tbprofile = mongoose.Schema({
    userId: String,
    name: String,
    url: String,
    network: String,
    focus: String,
});


module.exports = mongoose.model('profile',tbprofile);