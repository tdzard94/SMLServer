var mongoose = require('mongoose');

var tbssh = mongoose.Schema({
    userId: String,
    name: String,
    total: Number,
    sshs: [{
        ip: Number,
        username: String,
        pass: String
    }]
});


module.exports = mongoose.model('ssh',tbssh);