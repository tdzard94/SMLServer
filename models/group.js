var mongoose = require('mongoose');

var tbgroup = mongoose.Schema({
    userId: String,
    name: String,
    profile: String,
    proxy: String,
    useSocks: Boolean,
    status: String,
    action: String
});


module.exports = mongoose.model('group',tbgroup);