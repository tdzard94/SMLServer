var mongoose = require('mongoose');

var carrier = mongoose.Schema({
    ISO: String,
    COUNTRY: String,
    MCC: String,
    MNC: String,
    CODE: String,
    NETWORK: String
});
//BOARD,BRAND,DEVICE,DISPLAY,HARDWARE,ID,MANUFAC,MODEL,PRODUCT,HOST,INCREMEN,RELEASE
module.exports = mongoose.model('carrier',carrier);
