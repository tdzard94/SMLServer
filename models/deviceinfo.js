var mongoose = require('mongoose');

var dvSchema = mongoose.Schema({
    type: String,
    BOARD: String,
    BRAND: String,
    DEVICE: String,
    DISPLAY: String,
    HARDWARE: String,
    ID: String,
    MANUFAC: String,
    MODEL: String,
    PRODUCT: String,
    HOST: String,
    INCREMEN: String,
    RELEASE: String,
    IMEI: String,
    GLVENDOR: String,
    GLRENDERER: String,
    DPI: String
});
//BOARD,BRAND,DEVICE,DISPLAY,HARDWARE,ID,MANUFAC,MODEL,PRODUCT,HOST,INCREMEN,RELEASE
module.exports = mongoose.model('deviceinfo',dvSchema);

