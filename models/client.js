var mongoose = require('mongoose');

var tbclient = mongoose.Schema({
    userId: String,
    name: String,
    IP: String,
    group: String,
    dateCreate: String,
});

module.exports = mongoose.model('clients',tbclient);