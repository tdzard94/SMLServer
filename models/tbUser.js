var mongoose = require('mongoose'),
bcrypt  = require('bcrypt-nodejs'),
crypto = require('crypto');

var UserSchema = mongoose.Schema({
    username:String,
    password:String,
    isAdmin:Boolean,
    enable:Boolean,
    apiKey:String
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
UserSchema.methods.generateapiKey = function() {
    return crypto.randomBytes(16).toString('hex');
};

UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};



module.exports = mongoose.model('tbUser',UserSchema);