var mongoose = require('mongoose');

var setting = mongoose.Schema({
    userId: String,
    TIME_LOADLINK: Number,
    TIME_DOWNAPP: Number,
    TEXT_CLICK: String,
    TEXT_ERROR: String,
    WIPE_PACKAGE: String,
    DEFAULT_PACKAGE: String
});


module.exports = mongoose.model('setting',setting);