var mongoose = require('mongoose');

var scr = mongoose.Schema({
    userId: String,
    name: String,
    package: String,
    isDefault: Boolean,
    content: []
});

module.exports = mongoose.model('scripts',scr);