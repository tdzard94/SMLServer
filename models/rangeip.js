var mongoose = require('mongoose');

var tbrange = mongoose.Schema({
    userId: String,
    name: String,
    total: Number,
    ranges: [{
        start: Number,
        end: Number,
    }]
});


module.exports = mongoose.model('rangeip',tbrange);