var User = require('./models/tbUser');
var mongoose = require('mongoose');
var dbConfig = require('./config/database');
var DV = require('./models/deviceinfo');
var fs = require('fs');

mongoose.connect(dbConfig.url,(err)=>{
    if(err) console.log(err);
    console.log('Connected');
});



fs.readFile('dvif.csv', 'utf8', function (err, data) {
    if (err) throw err;
    data.split('\n').forEach(x=>{
        dvS = x.split(',');
        new DV({
            type:'Android',
            BOARD: dvS[0].toString(),
            BRAND: dvS[1].toString(),
            DEVICE: dvS[2].toString(),
            DISPLAY: dvS[3].toString(),
            HARDWARE: dvS[4].toString(),
            ID: dvS[5].toString(),
            MANUFAC: dvS[6].toString(),
            MODEL: dvS[7].toString(),
            PRODUCT: dvS[8].toString(),
            HOST: dvS[9].toString(),
            INCREMEN: dvS[10].toString(),
            RELEASE: dvS[11].toString(),
            IMEI:  dvS[12].toString(),
            GLVENDOR: dvS[13].toString(),
            GLRENDERER:  dvS[14].toString(),
            DPI:  dvS[15].toString().replace('\r','')
        }).save();

    })
});
