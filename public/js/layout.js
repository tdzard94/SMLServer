$(document).ready(()=>{
    $('li[name="sidebar"]').removeClass('active');
    console.log(active);
    if(!active)
        return;
    switch(active)
    {   
        case 'dashboard':
            $('li[onmenu="dashboard"]').attr('class','active');
            break;
        case 'apikey':
            $('li[onmenu="apikey"]').attr('class','active');
            break;
        case 'uaandroid':
            $('li[onmenu="devicemanager"]').attr('class','active');
            $('li[name="useragent"]').attr('class','active');
            $('li[par="useragent"][name="android"]').attr('class','active');
            break;
        case 'uaios':
            $('li[onmenu="devicemanager"]').attr('class','active');
            $('li[name="useragent"]').attr('class','active');
            $('li[par="useragent"][name="ios"]').attr('class','active');
            break;
        case "deviceandroid":
            $('li[onmenu="devicemanager"]').attr('class','active');
            $('li[name="deviceinfo"]').attr('class','active');
            $('li[par="deviceinfo"][name="android"]').attr('class','active');
            break;
        case "serialandroid":
            $('li[onmenu="devicemanager"]').attr('class','active');
            $('li[name="serial"]').attr('class','active');
            $('li[par="serial"][name="android"]').attr('class','active');
            break;
        case "serialios":
            $('li[onmenu="devicemanager"]').attr('class','active');
            $('li[name="serial"]').attr('class','active');
            $('li[par="serial"][name="ios"]').attr('class','active');
            break;
        case "campaign":
            $('li[onmenu="campaign"]').attr('class','active');
            break;
        case "scripts":
            $('li[onmenu="scripts"]').attr('class','active');
            break;
        case "rrsproxyssh":
            $('li[onmenu="taskmanager"]').attr('class','active');
            $('li[name="rrsclick"]').attr('class','active');
            $('li[par="rrsclick"][name="proxy"]').attr('class','active');
            $('li[par="proxy"][name="ssh"]').attr('class','active');
            break;
        case "rrsproxysocks":
            $('li[onmenu="taskmanager"]').attr('class','active');
            $('li[name="rrsclick"]').attr('class','active');
            $('li[par="rrsclick"][name="proxy"]').attr('class','active');
            $('li[par="proxy"][name="socks"]').attr('class','active');
            break;
        case "rrsproxyrange":
            $('li[onmenu="taskmanager"]').attr('class','active');
            $('li[name="rrsclick"]').attr('class','active');
            $('li[par="rrsclick"][name="proxy"]').attr('class','active');
            $('li[par="proxy"][name="rangeip"]').attr('class','active');
            break;
        case "taskrrs":
            $('li[onmenu="taskmanager"]').attr('class','active');
            $('li[name="rrsclick"]').attr('class','active');
            $('li[par="rrsclick"][name="task"]').attr('class','active');
            $('li[par="task"][name="rrs"]').attr('class','active');
            break;
        case "taskclick":
            $('li[onmenu="taskmanager"]').attr('class','active');
            $('li[name="rrsclick"]').attr('class','active');
            $('li[par="rrsclick"][name="click"]').attr('class','active');
            break;
        case "tasklead":
            $('li[onmenu="taskmanager"]').attr('class','active');
            $('li[name="lead"]').attr('class','active');
            break;
        case "inappinvoice":
            $('li[onmenu="inapp"]').attr('class','active');
            $('li[name="invoice"]').attr('class','active');
            break;
        case "inappdashboard":
            $('li[onmenu="inapp"]').attr('class','active');
            $('li[name="dashboard"]').attr('class','active');
            break;
        case "usermanager":
            $('li[onmenu="inapp"]').attr('class','active');
            $('li[name="usermanager"]').attr('class','active');
            break;
        case "inappreport":
            $('li[onmenu="inapp"]').attr('class','active');
            $('li[name="report"]').attr('class','active');
            break;
        case "inappclick":
            $('li[onmenu="inapp"]').attr('class','active');
            $('li[name="click"]').attr('class','active');
            break;
        //usermanager
    }
});