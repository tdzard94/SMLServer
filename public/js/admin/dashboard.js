var tb = $('table').DataTable();
var socket;

$(document).ready(()=>{
    socket = io();
    loadData();

    $('#modal-add').on('shown.bs.modal',()=>{
        $.ajax({
            url: '/dashboard/info',
            type: 'GET',
            success: (rs)=>{
                if(rs == null) location.reload();
                console.log(rs.data);
                $('#profile').html('');
                rs.data[0].forEach(e => {
                    $('#profile').append('<option value="'+e._id+'">'+e.name+'</option>');
                });
                $('#proxy').html('');
                rs.data[1].forEach(e => {
                    $('#proxy').append('<option value="'+e._id+'">'+e.name+'</option>');
                });
                $('#clients').html('');
                rs.data[2].forEach(e => {
                    $('#clients').append('<option value="'+e._id+'">'+e.name+' [group:'+e.group+']</option>');
                });
                
                $('.select2').select2();
            }
        })
    });

    $('#modal-edit').on('shown.bs.modal',()=>{
        $.ajax({
            url: '/dashboard/info',
            type: 'GET',
            success: (rs)=>{
                if(rs == null) location.reload();
                console.log(rs.data);
                $('#eprofile').html('');
                rs.data[0].forEach(e => {
                    $('#eprofile').append('<option value="'+e._id+'">'+e.name+'</option>');
                });
                $('#eproxy').html('');
                rs.data[1].forEach(e => {
                    $('#eproxy').append('<option value="'+e._id+'">'+e.name+'</option>');
                });
                $('#eclients').html('');
                rs.data[2].forEach(e => {
                    $('#eclients').append('<option value="'+e._id+'" group="'+e.group+'">'+e.name+' [group:'+e.group+']</option>');
                });

                $.ajax({
                    url: $('#efrmDv').attr('action'),
                    type:'get',
                    success: (rs)=>{
                        if(rs == null) location.reload();

                        console.log(rs);
                        $('#tit').text('EDIT ['+rs.data.name+']');
                        $('#eprofile').val(rs.data.profile);
                        $('#eproxy').val(rs.data.proxy);
                        $('option[group="'+rs.data.name+'"]').attr('selected',true);
                        $('.select2').select2(); 
                    }
                });

                $('.select2').select2(); 

            }
        })
    });

    $('#frmDv').submit(e=>{
        e.preventDefault();

        $.ajax({
            url: '/dashboard/add',
            type: 'POST',
            dataType: 'json',
            data:{
                name: $('#name').val(),
                profile: $('#profile').val(),
                proxy: $('#proxy').val(),
                clients: $('#clients').val(),
                useSocks:false
            },
            success: (rs)=>{
                if(rs==null) location.reload();
                console.log(rs);
                tb.row.add(['<td>'+ 
                '<div class="btn-group">'+
                    '<button type="button" class="btn btn-success btn-flat">Action</button>'+
                    '<button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">'+
                        '<span class="caret"></span>'+
                        '<span class="sr-only">Toggle Dropdown</span>'+
                    '</button>'+
                    '<ul class="dropdown-menu" role="menu">'+
                        '<li><a data-id="'+rs.data.id+'" onclick=_action(this,"start_lead") >Start Lead</a></li>'+
                        '<li><a data-id="'+rs.data.id+'" onclick=_action(this,"stop_lead") >Stop Lead</a></li>'+
                        '<li><a data-id="'+rs.data.id+'" onclick=_action(this,"restart") >Restart</a></li>'+
                    '</ul>'+
                '</div>'+
            '</td>',rs.data.name,rs.data.status,'<td><button type="button"  data-id="'+rs.data['_id']+'" onclick="_edit(this);" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i >Edit</button> <button type="button" data-id="'+rs.data['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button></td>']).draw(true);
                //$('#tbBody').append(row);
            $('#modal-add').modal('hide');
                
            }
        })
    });

    $('#efrmDv').submit(e=>{
        e.preventDefault();

        $.ajax({
            url: '/dashboard/edit',
            type: 'POST',
            dataType: 'json',
            data:{
                id: $('#efrmDv').attr('data-id'),
                profile: $('#eprofile').val(),
                proxy: $('#eproxy').val(),
                clients: $('#eclients').val(),
                useSocks:false
            },success: (rs)=>{
                if(rs==null) location.reload();
                
                $('#modal-edit').modal('hide');
            }
        })
    });
});


function loadData()
{
    $('#tbBody').html('');
    $.ajax({
        url:"/dashboard/getdata",
        type:'GET',
        success: (rs)=>{
            if(rs == null) location.reload();

            rs.data.forEach(x=>{
                tb.row.add(['<td>'+ 
                '<div class="btn-group">'+
                    '<button type="button" class="btn btn-success btn-flat">Action</button>'+
                    '<button type="button" class="btn btn-success btn-flat dropdown-toggle" data-toggle="dropdown">'+
                        '<span class="caret"></span>'+
                        '<span class="sr-only">Toggle Dropdown</span>'+
                    '</button>'+
                    '<ul class="dropdown-menu" role="menu">'+
                        '<li><a data-id="'+x._id+'" onclick=_action(this,"start_lead") >Start Lead</a></li>'+
                        '<li><a data-id="'+x._id+'" onclick=_action(this,"stop_lead") >Stop Lead</a></li>'+
                        '<li><a data-id="'+x._id+'" onclick=_action(this,"restart") >Restart VPS</a></li>'+
                    '</ul>'+
                '</div>'+
            '</td>',x.name,x.status,'<td><button type="button"  data-id="'+x['_id']+'" onclick="_edit(this);" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i >Edit</button> <button type="button" data-id="'+x['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button></td>']).draw(true);
            })
        }
    })
}

function _delete(obj)
{
    var bt = $(obj);

    $.ajax({
        url: '/dashboard/delete',
        type: 'POST',
        dataType: 'json',
        data:{
            id: bt.attr('data-id')
        },
        success: (rs)=>{
            if(rs == null || !rs.success) location.reload();

            tb.row($(bt).parents('tr')).remove().draw(true);
        }
    })
}

function _edit(obj)
{
    $('#efrmDv').attr('action','/dashboard/get/'+$(obj).attr('data-id'));
    $('#efrmDv').attr('data-id',$(obj).attr('data-id'));
    $('#modal-edit').modal('show');
}

function _action(obj,action)
{
    if(action != "restart")
    {
        $.ajax({
            url: '/dashboard/updateaction',
            type:'POST',
            dataType:'json',
            data:{
                action: action,
                id: $(obj).attr('data-id')
            },success: (rs)=>{
                console.log(rs);
            }
        });
    }
    socket.emit('send_action',{
        action: action,
        id: $(obj).attr('data-id')
    });
}