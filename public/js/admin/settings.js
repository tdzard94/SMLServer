$(document).ready(()=>{
    $("#timeLoad").TouchSpin({
        verticalbuttons: true,
        min:1,
        max: 100
    });
    $("#timeDown").TouchSpin({
        verticalbuttons: true,
        min:1,
        max: 65000
    });

    $.ajax({
        url: '/settings/get',
        type:'GET',
        success:(rs)=>{
            if(rs == null) location.reload();
            if(!rs.data)
                return;

            $('#timeLoad').val(rs.data.TIME_LOADLINK);
            $('#timeDown').val(rs.data.TIME_DOWNAPP);
            $('#dfpakg').val(rs.data.DEFAULT_PACKAGE);
            $('#wipepkg').val(rs.data.WIPE_PACKAGE);
            $('#click').val(rs.data.TEXT_CLICK);
            $('#error').val(rs.data.TEXT_ERROR);
        }
    })

    $('#frmDv').submit(e=>{
        e.preventDefault();

        $.ajax({
            url: '/settings/save',
            type: 'post',
            dataType: 'json',
            data:{
                timeLoad: $('#timeLoad').val(),
                timeDown: $('#timeDown').val(),
                dfpkg: $('#dfpakg').val(),
                click: $('#click').val(),
                error: $('#error').val(),
                wipepkg: $('#wipepkg').val()
            },success: (rs)=>{
                if(rs == null) location.reload();

                console.log(rs);
            }
        });
    });
});