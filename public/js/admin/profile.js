var tb = $('table').DataTable();

$(document).ready(()=>{
    loadData();
    var l = $('#btAdd').ladda();
    $('#esshs').html('');
    $('#erangeip').html('');
    $('#modal-add').on('show.bs.modal', (e=>{
        $('#name').val('');
        $('#url').val('');
        $.ajax({
            url: '/proxy/getlist',
            type:'GET',
            beforeSend: ()=>{
                l.ladda('start');
            },
            success: (rs)=>{
                if(rs == null) location.reload();
                console.log(rs);
                rs.data[0].forEach(e => {
                    $('#sshs').append('<option value="'+e._id+'">'+e.name+'</option>')
                });
                rs.data[1].forEach(e => {
                    $('#rangeip').append('<option value="'+e._id+'">'+e.name+'</option>')
                });
                l.ladda('stop');
            }
        })
    }));
    $('#modal-add').submit(e=>{
        e.preventDefault();

        $.ajax({
            url: '/profile/add',
            type: 'POST',
            dataType: 'json',
            data:{
                name: $('#name').val(),
                url: $('#url').val(),
                network: $('#network').val(),
                forcus: $('#forcusapp').val(),
            },
            beforeSend: ()=>{

            },success: (rs)=>{
                if(rs == null) location.reload();

                tb.row.add([tb.rows().count()+1,rs.data.name,rs.data.url,rs.data.network,'<button type="button"  data-id="'+rs.data['_id']+'" onclick="_edit(this);" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i >Edit</button> <button type="button" data-id="'+rs.data['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button>']).draw(true);
                $('#modal-add').modal('hide');
            }
        })
    });

    $('#modal-edit').submit(e=>{
        e.preventDefault();

        $.ajax({
            url: '/profile/edit',
            type: 'POST',
            dataType:'json',
            data:{
                url: $('#eurl').val(),
                sshs: $('#esshs').val(),
                rangeip: $('#erangeip').val(),
                platform: $('#eplatform').val(),
                id: $('#efrmDv').attr('data-id')
            },
            success: (rs)=>{
                if(rs == null || !rs.success) location.reload();

                $('#modal-edit').modal('hide');
            }
        })
    });
});

function loadData(){
    tb.clear();
    $.ajax({
        url: '/profile/getdata',
        type: 'GET',
        beforeSend: ()=>{

        },
        success: (rs)=>{
            if(rs == null) location.reload();
            console.log(rs);
            rs.data.forEach(x=>{
                tb.row.add([tb.rows().count()+1,x.name,x.url,x.network,'<button type="button"  data-id="'+x['_id']+'" onclick="_edit(this);" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i >Edit</button> <button type="button" data-id="'+x['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button>']).draw(true);
            });
        }
    })
}
function _delete(obj)
{
    var bt = $(obj);

    $.ajax({
        url: '/profile/delete',
        type: 'POST',
        dataType: 'json',
        data:{
            id: bt.attr('data-id')
        },
        success: (rs)=>{
            if(rs == null || !rs.success) location.reload();

            tb.row($(bt).parents('tr')).remove().draw(true);
        }
    })
}

function _edit(obj)
{
    var id = $(obj).attr('data-id');
    var l = $('#btEdit').ladda();

    $.ajax({
        url: '/proxy/getlist',
        type:'GET',
        beforeSend: ()=>{
            l.ladda('start');
        },
        success: (rs)=>{
            if(rs == null) location.reload();
            $('#esshs').html('');
            $('#erangeip').html('');
            rs.data[0].forEach(e => {
                $('#esshs').append('<option value="'+e._id+'">'+e.name+'</option>')
            });
            rs.data[1].forEach(e => {
                $('#erangeip').append('<option value="'+e._id+'">'+e.name+'</option>')
            });

            $.ajax({
                url: '/profile/get/'+id,
                type:'GET',
                beforeSend: ()=>{
                    l.ladda('start');
                },
                success: (rs)=>{
                    console.log(rs);
                    if(rs == null) location.reload();
                    $('#tit').text('EDIT ['+rs.data.name+']');
                    //$('#ename').val(rs.data.name);
                    $('#e_url').val(rs.data.url);
                    $('#e_network').val(rs.data.network);
                    $('#e_forcusapp').val(rs.data.forcus);
                    $('#efrmDv').attr('data-id',rs.data._id);
                    l.ladda('stop');
                }
            });

            
        }
    });

    $('#modal-edit').modal('show');
}