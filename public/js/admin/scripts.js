var tb = $('table').DataTable();

$(document).ready(()=>{
    loadData();

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });

    $('#frmDv').submit(e=>{
        e.preventDefault();

        $.ajax({
            url:'/scripts/add',
            type:'POST',
            dataType:'json',
            data:{
                name: $('#name').val(),
                package: $('#package').val(),
                df: $('#dfscr').prop('checked'),
                content: $('#content').val()
            },
            success: (rs)=>{
                if(rs == null) location.reload();

                if(!rs.success)
                    return alert('Name is not availble');
                
                tb.row.add(['#',rs.data.name,'<button type="button"  data-id="'+rs.data['_id']+'" onclick="_edit(this);" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i >Edit</button> <button type="button" data-id="'+e['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button>']).draw(true);
                $('#modal-add').modal('hide');
            }
            
        })
    });

    $('#efrmDv').submit(e=>{
        e.preventDefault();

        $.ajax({
            url: '/scripts/edit',
            type: 'post',
            dataType: 'json',
            data:{
                id: $('#efrmDv').attr('data-id'),
                package: $('#epackage').val(),
                isDefault: $('#edfscr').prop('checked'),
                content: $('#econtent').val()
            },
            success: (rs)=>{
                if(rs == null) location.reload();

                $('#modal-edit').modal('hide');
            }
        })
    });

    $('#modal-edit').on('shown.bs.modal',()=>{
        $.ajax({
            url: '/scripts/get/'+$('#efrmDv').attr('data-id'),
            type:'get',
            success: (rs)=>{
                if(rs == null) location.reload();

                if(!rs.success)
                    location.reload();
                
                $('#tit').text('EDIT ['+rs.data.name+']');
                $('#epackage').val(rs.data.package);
                $('#econtent').val(rs.data.content.join('\n'));
                $('#edfscr').prop('checked',rs.data.isDefault);

                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                  });
            }
        });
    });
});

function loadData()
{
    $.ajax({
        url: '/scripts/getlist',
        type:'GET',
        success: (rs)=>{
            if(rs == null) location.reload();

            tb.clear();
            rs.data.forEach(e => {
                tb.row.add(['#',e.name,'<button type="button"  data-id="'+e['_id']+'" onclick="_edit(this);" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i >Edit</button> <button type="button" data-id="'+e['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button>']).draw(true);
            });
            
            $('#modal-add').modal('hide');
        }
    })
}

function _delete(obj)
{
    $.ajax({
        url: '/scripts/delete',
        type:'POST',
        dataType: 'json',
        data:{
            id: $(obj).attr('data-id')
        }
        ,success: (rs)=>{
            if(rs == null) location.reload();

            if(!rs.success)
                location.reload();

            tb.row($(obj).parents('tr')).remove().draw(true);
        }
    })
}

function _edit(obj)
{
    $('#name').val('');
    $('#package').val('');
    $('#dfscr').prop('checked',false);
    $('#content').val('');
    $('#efrmDv').attr('data-id',$(obj).attr('data-id'));
    $('#modal-edit').modal('show');
}
