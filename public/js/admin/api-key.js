
$(document).ready(()=>{
    $.get('api-key/getkey',(data)=>{
        $('#key').val(data['key']);
    });
    $('#btCoppy').click(()=>{
        $('#key').CopyToClipboard();
    });
    $('#btGen').click(()=>{
        $.get('api-key/genkey',(data)=>{
            $('#key').val(data['key']);
        });
    });
});