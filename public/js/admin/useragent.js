var editor = CodeMirror.fromTextArea(document.getElementById("uaTxt"), {
    lineNumbers: true,
    matchBrackets: true,
    styleActiveLine: true,
    theme:"ambiance"
});

$(document).ready(()=>{
    
    console.log(uaType);
    loadData();

    $('#btSave').click(()=>{
        saveData();
    })
});

function loadData()
{
    $.ajax({
        url: '/useragent/get/'+uaType,
        type: 'GET',
        success: (rs)=>{
            if(rs == null) location.reload();

            if(rs.success)
            {
                var ua = rs.data.join('\n');
                editor.setValue(ua);
            }else
                return;
        }
    })
}

function saveData()
{
    $.ajax({
        url: '/useragent/add',
        type:'POST',
        data:{
            type: uaType,
            content: editor.getValue(),
        },
        success: (rs)=>{
            if(rs == null) location.reload();
            console.log(rs);
        }
    })
}