var tb = $('table').DataTable();

$(document).ready(()=>{
    
    loadData();
});

function loadData()
{
    console.log('loda data');
    $.ajax({
        url:'/client/getdata',
        type: 'GET',
        success: (rs)=>{
            if(rs == null) location.reload();
            $('#tbBody').html('');
            rs.data.forEach(x => {
                var html =  '<tr>'+
                                '<td>#</td>'+
                                '<td>'+x.name+'</td>'+
                                '<td>'+x.IP+'</td>'+
                                '<td>'+x.dateCreate+'</td>'+
                                '<td><button type="button" data-id="'+x['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button></td>'+
                            '</tr>';
                
                $('#tbBody').append(html)
            });
        }
    })
}

function _delete(obj)
{
    $.ajax({
        url: '/client/delete',
        type: 'POST',
        dataType: 'json',
        data:{
            id: $(obj).attr('data-id')
        },success: (rs)=>{
            if(rs == null) location.reload();

            if(!rs.success)
            {
                loadData();
                return;
            } 

            $(obj).parent().parent().remove();
        }
    })
}