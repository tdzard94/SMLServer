var tb = $('table').DataTable();
$(document).ready(()=>{
    loadData();

    $('#frmDv').submit(e=>{
        e.preventDefault();
        console.log('submit');
        $.ajax({
            url:'/proxy/add',
            type: 'POST',
            dataType: 'json',
            data:{
                name: $('#name').val(),
                content: $('#txtContent').val(),
                type: prType
            },
            beforeSend: ()=>{

            },
            success: (rs)=>{
                if(rs == null) location.reload();

                console.log(rs);
                if(rs.success)
                {
                    if(rs.isDup)
                    {
                        alert('Name is exist, please input another name');
                        return;
                    }

                    tb.row.add([tb.rows().count()+1,rs.data.name,rs.data.count,'<button type="button"  data-id="'+rs.data['_id']+'" onclick="_edit(this);" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i >Edit</button> <button type="button" data-id="'+rs.data['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button>']).draw(true);
;                }
                $('#modal-add').modal('hide');
            }
        })
    });

    $('#efrmDv').submit(e=>{
        e.preventDefault();

        $.ajax({
            url: '/proxy/edit',
            type:'POST',
            dataType: 'json',
            data:{
                type: prType,
                id: $('#efrmDv').attr('data-id'),
                content: $('#etxtContent').val()
            },success: (rs)=>{
                if(rs == null) location.reload();
                $('#modal-edit').modal('hide');
            }
        });
    });
});

function loadData()
{
    tb.clear();
    $.ajax({
        url: '/proxy/get/'+prType,
        type: 'GET',
        beforeSend: ()=>{

        },
        success: (rs)=>{
            if(rs == null) location.reload();

            if(!rs.success)
                return;

            console.log(rs);
            var i = 1;
            rs.data.forEach(e => {
                tb.row.add([i,e.name,e.total,'<button type="button"  data-id="'+e['_id']+'" onclick="_edit(this);" class="btn btn-info btn-xs"><i class="fa fa-fw fa-edit"></i >Edit</button> <button type="button" data-id="'+e['_id']+'"  onclick="_delete(this);" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-close"></i >Delete</button>']).draw(true);
                i++;
            });
        }
    });
}

function _delete(obj)
{
    var bt = $(obj);
    console.log(bt.attr('data-id'));
    $.ajax({
        url: '/proxy/delete',
        type: 'POST',
        dataType: 'json',
        data:{
            type: prType,
            id: bt.attr('data-id')
        },
        success: (rs)=>{
            if(rs == null) location.reload();

            console.log(rs);
            if(!rs.success)
                location.reload();

            tb.row($(bt).parents('tr')).remove().draw(true);
        }
    });
}

function _edit(obj)
{
    var bt = $(obj);
    var url = '/proxy/'+prType+'/get/'+bt.attr('data-id');
    console.log(url);
    $.ajax({
        url: '/proxy/'+prType+'/get/'+bt.attr('data-id'),
        type:'GET',
        success:(rs)=>{
            if(rs == null) location.reload();
            console.log(rs);
            $('#tit').text('EDIT '+rs.data.name);
            var ssh = [];
            if(prType == "ssh")
            {
                rs.data.sshs.forEach(x=>{
                    ssh.push(int2ip(x.ip)+'|'+x.username+'|'+x.pass);
                });
            }else
            {
                rs.data.ranges.forEach(x=>{
                    ssh.push(int2ip(x.start)+'|'+int2ip(x.end));
                });
            }
            
            $('#efrmDv').attr('data-id',bt.attr('data-id'));
            $('#etxtContent').val(ssh.join('\n'));
            $('#modal-edit').modal('show');
        }
    })
}

function int2ip (ipInt) {
    return ( (ipInt>>>24) +'.' + (ipInt>>16 & 255) +'.' + (ipInt>>8 & 255) +'.' + (ipInt & 255) );
}
