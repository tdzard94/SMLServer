$(document).ready(()=>{
    var checked = localStorage.getItem('isRemember');
   
    if(checked)
    {
        $('#rem').iCheck('check');
        $('input[name="username"]').val(localStorage.getItem('Account'));
        $('input[name="password"]').val(localStorage.getItem('Password'));
    }

    $('#rem').on('ifChanged',(e)=>{
        var ischecked = e.target.checked;
        localStorage.setItem('isRemember',ischecked);
    });

    $('#frm').submit(()=>{
        var check = $('#rem').prop('checked');
        if(check)
        {
            localStorage.setItem('Account',$('input[name="username"]').val());
            localStorage.setItem('Password',$('input[name="password"]').val());
        }else{
            localStorage.removeItem("Account");
            localStorage.removeItem("Password");
            localStorage.removeItem("isRemember");
        }
    });
});