var User = require('./models/tbUser');
var mongoose = require('mongoose');
var dbConfig = require('./config/database');
var CARRIERS = require('./models/carrier');
var fs = require('fs');

mongoose.connect(dbConfig.url,(err)=>{
    if(err) console.log(err);
    console.log('Connected');
});



fs.readFile('carrier.csv', 'utf8', async function (err, data) {
    if (err) throw err;
    await data.split('\n').forEach(x=>{
        dvS = x.split('|');
        new CARRIERS({
            MCC: dvS[0],
            MNC: dvS[1],
            ISO: dvS[2],
            COUNTRY: dvS[3],
            CODE: dvS[4],
            NETWORK:dvS[5].replace('\r','')
        }).save();
    });
    console.log('done');
});
