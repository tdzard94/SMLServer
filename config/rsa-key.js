var server = {
    public_key: "-----BEGIN PUBLIC KEY-----\n"+
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq9Y7LdF5jRygOZsFcu/B\n"+
    "RRkytGQulnqtMFYE4WGOHlSm/KJbvhOgJDN4hpqj7WP1JY3vM/EPlZO8vNPdI0Im\n"+
    "7G+Cvq9hEDNLGN5gD/1HKoOFoCFSiE7CeYGAnh5i3enpikDBrnNcfGgP1kDgMvm2\n"+
    "nq+D6ml+0T2sZsmLuO9OBBL42Cn03vXmahcKVAhh/ghEaYNmPhgJvDLz0OyVfJQS\n"+
    "5l4LzNP5E5rn8HHsoey6q0q7Q6cCYwK5KkNd5IPp64lJNSfZaf4IDt6t77MTUBYy\n"+
    "CyrLQ+7BmxFsKEIcz0dPdyX274cOg5PXZbfT3iTLnJQuJ2jI2XHDd4Od5LkU9EmJ\n"+
    "zQIDAQAB\n"+
    "-----END PUBLIC KEY-----",


    private_key:
    "-----BEGIN RSA PRIVATE KEY-----\n"+
    "MIIEogIBAAKCAQEAq9Y7LdF5jRygOZsFcu/BRRkytGQulnqtMFYE4WGOHlSm/KJb\n"+
    "vhOgJDN4hpqj7WP1JY3vM/EPlZO8vNPdI0Im7G+Cvq9hEDNLGN5gD/1HKoOFoCFS\n"+
    "iE7CeYGAnh5i3enpikDBrnNcfGgP1kDgMvm2nq+D6ml+0T2sZsmLuO9OBBL42Cn0\n"+
    "3vXmahcKVAhh/ghEaYNmPhgJvDLz0OyVfJQS5l4LzNP5E5rn8HHsoey6q0q7Q6cC\n"+
    "YwK5KkNd5IPp64lJNSfZaf4IDt6t77MTUBYyCyrLQ+7BmxFsKEIcz0dPdyX274cO\n"+
    "g5PXZbfT3iTLnJQuJ2jI2XHDd4Od5LkU9EmJzQIDAQABAoH/C2kuhNxkNfqwqSKi\n"+
    "Of+x1eTWeSQt89AxsAQGTvErftggPqaJcRXcjIredhHO0PTSUqwaNljLsCMfZWqs\n"+
    "RKsf6pN9GyyKRYkic4RQ3L70+t3BAeE0mdihEXCIGffqmNHvsD6cnfkLKze91Xx5\n"+
    "ZukuN1twZyCaS0rNKAY6QQ2jIKIF7SjQ6Yi5oKDVoZarEtnC4Ur9Si2wjwyHO7D+\n"+
    "AuSHjX0QdcELjBZn+4thvWFrZJF09eVGUqgzyVlhnl0iiDfxrgIN7yRmNswYDGLa\n"+
    "n+RvbFvxpJEscnls8ttQQLcZwJ0O8jH7DWrBvZdV4dc+oRf5YBg11cd2xfL4/xUf\n"+
    "e5D9AoGBAOUwisF3j9ZCIeVl3n7gtXsSe1OIICIiVFaOKvCe+1ObxX9v1Y9CTXnb\n"+
    "GdzTw+AS89+MrtBNblsd+EncYsdeDl0OwFYeX6PnjkjD0ID4c3rB6nqqK9UYJDc0\n"+
    "8pO8/CruiVX2Xnwsispnz+lqHuLHjPdhSE4vZjuhtEJomkrxdfyHAoGBAL/wKpu8\n"+
    "epVAwQLnywZRvq9HKCgNywms3U23620bkJp3gDLJI/veXTDKywuGcb/j0ZeEfC7L\n"+
    "UDlAOX3t2Y2IKHU2L0fuThN+tStCxeyIodCms0GDAexL/i6UtP/krzub+dTIKay6\n"+
    "dVXpm3np1tKx0PN5zF2bJSUp8U0/De6tJtALAoGBANmbclNpAw/q8KYPR+bt0kIN\n"+
    "dT0FhYUDVVJtX8XhHT3wK9/ovjtsN4WYi4DJLDSHqje6Ko0qx7oJxaHTd5JV8otW\n"+
    "ozXhVuuXwtfB3CloPLT0g35+LmoApvon8p5llIPqwkj1SBbcb9boJFc55CNsVqLX\n"+
    "IkDh7k52JWHcUYsQofbnAoGAW70S4HTpskTM+/KGG3MezPAZxWsyTGGPNQJfSDsQ\n"+
    "loPfhmaVCGFz8Uo8WjncTHc7SkxMfqE2quaDlCkG/prBm6RWXO8N6kt5lD/KEhAM\n"+
    "J4pFy14uoR6BUwpIqLZI+xjGBGnXYXONeY66fT7DKtlKodTpVSQ+UNEHQICOzDF+\n"+
    "h5kCgYEAm/teXPgzDa+OcJmJN83ytk2S1ftzVE1jndPl3CQkSPlbvnqQ0p/e0dAP\n"+
    "62usIcUlL5TOvACXVuVS3ow25XGj1m1L4oFzW73PZJK0r+HEqk0Ig1Gd4orSThtQ\n"+
    "dC8ZBjO08/w9jw6lq0bVtoACVZNQGWwpLR7RqUU1FNAI/sVnbvw=\n"+
    "-----END RSA PRIVATE KEY-----"
}

var client = {
    public_key: 
    "-----BEGIN PUBLIC KEY-----\n"+
    "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgitn4XnrVA8aGREvq2k8\n"+
    "Qraj5SPJZwCds1GQ9Vn5z/VRglQSsA6H7SvGDsQrSUKDi5YOnAUn0a1pShzc7U4S\n"+
    "TIg+LJna348TD7U4/7Xg1aPgLzogjQnXf4dqzAxXTekSuknqggL6TcPBhIH7+knY\n"+
    "1K1GXSYfJ3hMmdNMD50h+wf9w1u3RuEpfZd3/ZMMgvPgAcZyhTguWxw1zuyW7/hr\n"+
    "brwjusMVMogQry3R2OvKsO3YCn3z2rBZSIQbn9P5WwNwy5O3W9zndIRC+WIEOsL9\n"+
    "QA67T+NyOByOdd4tnsDlwgF+AEsCvVvu1MD5LDC3TL9dBcPgl3VCVrsLMp/Vhg8V\n"+
    "owIDAQAB\n"+
    "-----END PUBLIC KEY-----",


    private_key:
    "-----BEGIN RSA PRIVATE KEY-----\n"+
    "MIIEogIBAAKCAQEAgitn4XnrVA8aGREvq2k8Qraj5SPJZwCds1GQ9Vn5z/VRglQS\n"+
    "sA6H7SvGDsQrSUKDi5YOnAUn0a1pShzc7U4STIg+LJna348TD7U4/7Xg1aPgLzog\n"+
    "jQnXf4dqzAxXTekSuknqggL6TcPBhIH7+knY1K1GXSYfJ3hMmdNMD50h+wf9w1u3\n"+
    "RuEpfZd3/ZMMgvPgAcZyhTguWxw1zuyW7/hrbrwjusMVMogQry3R2OvKsO3YCn3z\n"+
    "2rBZSIQbn9P5WwNwy5O3W9zndIRC+WIEOsL9QA67T+NyOByOdd4tnsDlwgF+AEsC\n"+
    "vVvu1MD5LDC3TL9dBcPgl3VCVrsLMp/Vhg8VowIDAQABAoIBAEtbPYQ2xJmhNIaW\n"+
    "gHZHMe+dNDyO/yV2IdWRvs2LDSfq6Qj+CuOJRf9ze/FId5atvuGsOUDxax/juDSD\n"+
    "iRKIQ+eyZ+wCFceeWHabfyfp7wM/H6W3jOiZbfwSNS1zZ4dfCsx1xiEGAEnMrdc8\n"+
    "ONeosLJOrZF3ZEnBRvc2b64KVYWxQyuYHa7V4yE1hsLalsumtF1xFkalMvKx+Hw5\n"+
    "CJ/sWX8vkgfWCNqaQbbkm7Akvqxf3eHS7XVASVqeVYZ4NA+BFuoHb/T0VY5yBgZc\n"+
    "5SO4pKfE5rkziAhnN3UJggbe842rkWkIaSXLyjX9Olz+RC9tgsOwgEL7zVeK41q5\n"+
    "xfs7RQECgYEAvHeTKcxCxshXWGr0VJvXdAlxrP8U2gPMUlSDcIJi+tIes5G6esuH\n"+
    "3F0VVRm3X6nJ7iv7wd+EpXNYN2RDjLzdGIQSIBLnRpOd2BnA9rbEosc0Le/Ao3OL\n"+
    "Zi/iHKiXJshSLmxLyCRHilOIeMvol+Kdk54rqrhFOGh0PMUaajhz3yECgYEAsNAX\n"+
    "1zIZeUAwxCe0HDyGMgWE+kOjYtzFdmWXWesic/YwAzSqdRkPSiXX6Ors9OKbDyGE\n"+
    "7RMScwKZAtV585i+eJefpPdd5poUyHjish+ULBm7AtlHXuv6OvLgPvMwBK+vJ7W4\n"+
    "4NdkqSfAsMf5FozpHrY/k15dMNrQtKQ45dS1sEMCgYB7TkiMakWuRNlQ0fk1ehjG\n"+
    "pH8pTmLYGPAPVZgBfm0/6UWI+ulk+dDTzlrsqLye7DlfiItY1uK0VPj8lK7VX6PQ\n"+
    "LGinb3a9aEs3XfIv+HhCf86V12zUA/7KRr3LPkpWZ23NTbFvKr1btTduwQpWhT/P\n"+
    "mwa61YEEXOLf951GrdSxwQKBgDil4w2CMqEjTLx7z/u/9GqwVANuijlRbD5bnBsl\n"+
    "93rZpFZSmE4+AiUKbNtl8RcgRveZEKJ4GtAYQ/ehd5nxFQ1D3tUWn9ITCZeVC2Rw\n"+
    "M6PRdpYaEQC7cAdU6z2f4oGN2/tIAHyg5dJANz8sQoTUDphVYZcQg6D0aiuF9ejE\n"+
    "tpVLAoGAAquMX7x57Nii+Q97Tvy8UPvC9ThsH/2X/RVp9omsTNBN2AOmQcZvch5k\n"+
    "PVIRXDb6DXfSeFQFAALXX1U2GYZijNag37fTPcJgC9kiEvmRM0L8+k9PGPJWVTXP\n"+
    "LT6io8zl5k5JUXsPbMpNQFY6541iJiBe1/TNJJ3zkyg0Rw/aVVs=\n"+
    "-----END RSA PRIVATE KEY-----"
}

module.exports.server = server;
module.exports.client =client;
