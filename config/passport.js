var passport = require('passport'),
LocalStrategy = require('passport-local').Strategy;
var tbUser = require('../models/tbUser');


passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
  passport.deserializeUser(async function(id, done) {
    var adUser = await tbUser.findById(id).exec();
    if(adUser)
    {
        return done(null, adUser);
    }

    //var iuser = await inappUser.findById(id).exec();
    //return done(null, iuser);
    // tbUser.findById(id, function(err, user) {
    //     if(err) {
    //       console.error('There was an error accessing the records of' +
    //       ' user with id: ' + id);
    //       return console.log(err.message);
    //     }
        
    //   })
    
  });

passport.use('user-login',new LocalStrategy({
    usernameField : 'username',
    passwordField : 'password',
    passReqToCallback : true
},(req,_username,_password,done)=>{
    tbUser.findOne({username: _username},(err,_user)=>{
        
        if(err) return done(err);
        if(!_user)
            return done(null,false,{errMsg: 'Incorrect username or password',username: _username, password: _password});
        if(!_user.validPassword(_password,_user.password))
            return done(null,false,{errMsg: 'Incorrect username or password',username: _username, password: _password});
        return done(null,_user);
    });
}));

passport.use('local-login', new LocalStrategy({
    usernameField : 'username',
    passwordField : 'password',
    passReqToCallback : true
},
function(req, uname, password, done) {
    tbUser.findOne({username: uname}, function(err, user) {
        if(err) {
          return errHandler(err);
          }
        if(!user) {
          return done(null, false, {errMsg: 'User does not exist, please' +
          ' <a class="errMsg" href="/signup">signup</a>'});
          }
        if(!user.validPassword(password)) {
          return done(null, false, {errMsg: 'Invalid password try again'});
          }
        return done(null, user);
    });

}));

module.exports = passport;