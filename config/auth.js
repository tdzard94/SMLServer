module.exports = {
    isLoggedIn: (req, res, next) => {
        if (req.isAuthenticated())
            return next();
        res.redirect('/login');
    },

    isAdmin: (req,res,next)=>{
        if (req.isAuthenticated())
        {
            if(req.user.isAdmin)
                return next();
            else
                res.send('You have not permission, pleas login with admin account');
        }
        res.redirect('/login');    
    },
    isUser: (req,res,next)=>{
        if (req.isAuthenticated())
        {
            if(!req.user.isAdmin)
                return next();
            else
                res.send('You have not permission, pleas login with admin account');
        }
        res.redirect('/login');  
    },
    isLogout: (req, res, next) => {
        if (!req.isAuthenticated() && !req.user.isAdmin)
        {
            res.json(null);
            return;
        }
        next();
    },
    isInappUser: (req, res, next) => {
        if (req.isAuthenticated())
        {
            if(!req.user.isAdmin)
                return next();
            else
                res.send('You have not permission, pleas login with admin account');
        }
        res.redirect('/inapp/user/login');    
    },
    isInappLogout: (req, res, next) => {
        if (!req.isAuthenticated() && req.user.isAdmin)
        {
            res.json(null);
            return;
        }
        next();
    },

}