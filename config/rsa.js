var NodeRSA = require('node-rsa');

var options = {
    encryptionScheme: {
        scheme: 'pkcs1', //scheme
        hash: 'md5', //hash using for scheme
      }
  }

function encrypt(plainText,publicKey)
{
    var encryptKey = new NodeRSA(publicKey,options);
    var encryptText = encryptKey.encrypt(plainText,"base64");
    return encryptText;
}

function decrypt(cipherText,privateKey)
{
    var decryptKey = new NodeRSA(privateKey,options);
    var decryptText = decryptKey.decrypt(cipherText,"utf-8");
    return decryptText;
}

module.exports.encrypt = encrypt;
module.exports.decrypt = decrypt;